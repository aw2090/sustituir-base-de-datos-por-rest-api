from flask import Flask,request, jsonify,Response
from flask_pymongo import PyMongo
from bson import json_util
from bson.objectid import ObjectId
from flask_cors import CORS, cross_origin


app = Flask(__name__)
app.config['MONGO_URI']='mongodb://localhost/word'
mongo = PyMongo(app)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'



@app.route('/api/words',methods=['GET'])
@cross_origin()
def Index():
    #metodo para traer todos los metodo
    words = mongo.db.words.find()
    response = json_util.dumps(words)
    return Response(response,mimetype='application/json')

@app.route('/api/words',methods=['POST'])
@cross_origin()
def add_word():
    
    word=request.json['word']
    description=request.json['description']

    #verifica si la fecha que el usuario ingreso ya habia sido registrada
    words = mongo.db.words.find_one({'word':word})
    
    ##Si la fecha habia sido registrada
    if (words):
        response = jsonify( {'message':'La palabra ya existe'})
        response.status_code = 400
        return response
        
    if word and description:
        #lo guarda en la bae de datos
        id = mongo.db.words.insert(
                
            {
                'word':word,
                'description':description,
            }
                
        )
        #se crea una respuesta para la api
        response = {
            'word':word,
            'description':description,

        }
            
        return response
    
    return {'message':'Error'}

@app.route('/api/words/<string:id>',methods=['DELETE'])
@cross_origin()
def delete_word(id):
    #verifica si la cita no  existe igual que en el metodo anterior, si no exite manda un error 400
    word=  mongo.db.words.find_one({'_id':ObjectId(id)})
    if (not word):
        response = jsonify( {'message':'La palabra no exite'})
        response.status_code = 400
        return response
    
    #Elimina la cita
    mongo.db.words.delete_one({'_id':ObjectId(id)})
    return jsonify({'message':'La palabra fue eliminada exitosamente'})


@app.route('/api/words/<string:id>',methods=['GET'])
@cross_origin()
def edit_word(id):
    
    #verifica si la fecha que el usuario ingreso ya habia sido registrada
    words = mongo.db.words.find_one({'word':word})
    
    ##Si la fecha habia sido registrada
    if (words):
        response = jsonify( {'message':'La palabra ya existe'})
        response.status_code = 400
        return response
    
    #metodo para traer todos los metodo
    words = mongo.db.words.find_one({'_id':ObjectId(id)})
    response = json_util.dumps(words)
    return Response(response,mimetype='application/json')
    

@app.route('/api/words/<id>', methods = ['PUT'])
@cross_origin()
def update_word(id):
    word=request.json['word']
    description=request.json['description']
    
    if  word and description:
        mongo.db.words.update_one( {'_id':ObjectId(id)} , {'word':word,'description':description})
        return {'message':'Datos actualizados con exito'}

    return {'message':'Error'}
   
#si la ruta no existe manda este error
@app.errorhandler(404)
@cross_origin()
def not_found(error=None):
    response = jsonify( {
        
        'message':'Recurso no encontrado',
        'status':404
    })
    response.status_code = 404
    return response

if __name__ == "__main__":
    app.run(host='0.0.0.0')